'use strict'

const router = require('express').Router();
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");

const userModel = require("../models/users");
const auth = require('../middleware/auth');

// register
router.post("/register", async(req,res)=>{
    try {
        let email = req.body.email;
        let phone_number = req.body.phone_number;
        let username = req.body.username;
        const email_detail = await userModel.find({"email": email}).exec();
        if(email_detail.length>0){
            return res.json({"status": "Failed", "message": "email already exists"});
        }
        const mobile_Availab = await userModel.find({"phone_number": phone_number}).exec()
        if(mobile_Availab.length>0){
            return res.json({"status": "Failed", "message": "phone_number already exists"});
        }
        const username_availabilty = await userModel.find({"username": username}).exec()
        if(username_availabilty.length>0){
            return res.json({"status": "Failed", "message": "Username already exists"});
        }
        let users = new userModel(req.body);
        if(req.body.password){
            let password = req.body.password;
            let salt = await bcrypt.genSalt(10);
            users.password = bcrypt.hashSync(password, salt);
            const user_details = await users.save();
            return res.json({"status": "Success", "message": "Register successfully", "data": user_details});
        }else{
            return res.json({"status": "Failed", "message": "Please Provide password"});
        }
    } catch (error) {
        return res.json({"status": "Failed","message": error.message});
    }
});


// login
router.post("/login", async(req,res)=>{
    try{        
        let username = req.body.username;
        let password = req.body.password;
        let users = await userModel.findOne({username: username}).exec();
        if(!users){
            throw new Error("No users found, please register");
        }
        let pass = users.password
        let match = await bcrypt.compare(password, pass);
        let token = users.generateToken();  
    
        if(match){
            return res.status(200).json({"status": "Success", "message": "Login successfully", "data": {...users._doc,token}});
        }else{
            return res.status(200).json({"status": "Failed", "message": "Invalid Username or password"});
        } 
    }catch(err){
        return res.status(400).json({"status": "Failed", "message": err.message});
    }
});

// add the user
router.post("/addTheUser", auth,  async(req,res)=>{
    try {
        let name = req.body.name;
        let phone_number = req.body.phone_number;
        const mobile_Availab = await userModel.find({"phone_number": phone_number}).exec()
        if(mobile_Availab.length>0){
            return res.json({"status": "Failed", "message": "phone_number already exists"});
        }else{
            const user = new userModel(req.body);
            user.created_by = req.user.uuid;
            const user_details = await user.save();
            return res.json({"status": "Success", "message": "User added successfully", "data": user_details});
        }
    } catch (err) {
        return res.status(400).json({"status": "Failed", "message": err.message});
    }
})

// get the all users
router.get("/getAllUsers", auth, async(req,res)=>{
    try{
        const data = await userModel.find().select("-password").exec();
        if(data.length > 0){
            return res.status(200).json({"status": "Success", "message": "User details successfully fetched", "data": data});
        }else{
            return res.status(204).json({"status": "Success", "message": "No User details found"});
        }
    }catch(err){
        return res.status(400).json({"status": "Failed", "message": err.message});
    }
});

// update the users
router.put("/updateUser", auth, async(req,res)=>{
    try {
        const condition = {"uuid" :req.body.uuid};
        const update = req.body
        const option = {new: true}
        const data = await userModel.findOneAndUpdate(condition, update, option).exec();
        return res.status(200).json({"status": "success", "message": "User details updated successfully", "data": data});
    } catch (err) {
        return res.status(400).json({"status": "Failed", "message": err.message});
    }
});

// delete the user
router.delete("/deleteUser/:uuid", auth, async(req,res)=>{
    try{
        const data = await userModel.findOneAndRemove({"uuid": req.params.uuid}).exec();
        return res.status(200).json({"status": "success", "message": "User details successfully deleted"});
    }catch(err){
        return res.json({"status": "Failed", message: err.message});
    }
});

module.exports = router;